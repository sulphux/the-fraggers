﻿using UnityEngine;
using System.Collections;

public class PlayerAnimator : MonoBehaviour
{
    #region Inspector Variables
    [SerializeField]
    private Animator _Animator;

    [SerializeField]
    private GameObject _FlagPrefab;

    [Header("Animation")]
    [SerializeField]
    private GameObject _FlagObject;
    [SerializeField]
    private Transform _FlagTransform;
    #endregion Inspector Variables

    #region Public Methods
    public bool CanMove()
    {
        var info = _Animator.GetCurrentAnimatorStateInfo(0);
        return !info.IsName("astronaut_put_flag");
    }

    public void Run()
    {
        _Animator.SetBool("running", true);
    }
    public void Walk()
    {
        _Animator.SetBool("running", false);
    }
    public void Flag()
    {
        _Animator.CrossFade("astronaut_put_flag", 0.0f);
    }

    public void Speed(float speed)
    {
        _Animator.SetFloat("speed", speed);
    }
    #endregion Public Methods

    #region Animation Callbacks
    public void OnShowFlag()
    {
        _FlagObject.SetActive(true);
    }
    public void OnPutFlag()
    {
        CreateFlag();
        _FlagObject.SetActive(false);
    }
    #endregion Animation Callbacks

    #region Private Methods
    private void CreateFlag()
    {
        var obj = Instantiate(_FlagPrefab, _FlagTransform.position, _FlagTransform.rotation) as GameObject;
    }
    #endregion Private Methods
}