using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Spawner : Photon.PunBehaviour
{
    public float playerSpawnRadius;
    public float tokenSpawnRadius;

    public int numberOfTokens = 10;

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        SpawnPlayer();
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();

        for (int i = 0; i < numberOfTokens; i++)
            SpawnToken();
    }

    public GameObject SpawnPlayer()
    {
        return PhotonNetwork.Instantiate("Player", _PlayerRandomSpawnPosition(), Quaternion.identity, 0);
    }

    public GameObject SpawnToken()
    {
        return PhotonNetwork.Instantiate("Token", _TokenRandomSpawnPosition(), Quaternion.identity, 0);
    }

    private Vector3 _PlayerRandomSpawnPosition()
    {
        return _RandomizePlanetSpawnPoint() * playerSpawnRadius;
    }

    private Vector3 _TokenRandomSpawnPosition()
    {
        return _RandomizePlanetSpawnPoint() * tokenSpawnRadius;
    }

    private Vector3 _RandomizePlanetSpawnPoint()
    {
        Vector3 tmp = UnityEngine.Random.rotation.eulerAngles.normalized;

        //x, y, z of tmp value will be always positive so they are need to be randomized too

        if (UnityEngine.Random.Range(0.0f, 100.0f) > 50.0f)
            tmp.x = -tmp.x;

        if (UnityEngine.Random.Range(0.0f, 100.0f) > 50.0f)
            tmp.y = -tmp.y;

        if (UnityEngine.Random.Range(0.0f, 100.0f) > 50.0f)
            tmp.z = -tmp.z;

        return tmp;
    }

    void OnDrawGizmos()
    {
        // Player Spawn Gizmo
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, playerSpawnRadius);

        // Token Spawn Gizmo
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, tokenSpawnRadius);

    }
}
