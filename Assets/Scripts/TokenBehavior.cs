using UnityEngine;
using System.Collections;

public class TokenBehavior : Photon.PunBehaviour
{

    #region Inspector Variables
    [Header("References")]
    public PhotonView tokenView;
    [SerializeField] private Spawner _Spawner;

    [SerializeField] private GameObject Cone1;
    [SerializeField] private GameObject Cone2;
    [SerializeField] private GameObject Cone3;
    [SerializeField] private GameObject Cube1;

    [Header("Properties")]
    public float RotationSpeed = 80.0f;

    #endregion Inspector Variables

    #region Private Variables
    private bool _isFalling = true;
    
    #endregion Private Variables

    // Use this for initialization
    void Start () {
        _isFalling = true;
	}
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Token"))
            return;
        
        if(other.transform.name == "Player" || other.transform.tag == "Player")
        {
            other.GetComponent<NetworkPlayerSkirmish>().AddScore();
            SpawnNew();
            Destroy();
        }

        _isFalling = false;
    }
    

    // Update is called once per frame
    void Update () {
        Cone1.transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed);
        Cone2.transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed);
        Cone2.transform.Rotate(Vector3.forward, Time.deltaTime * RotationSpeed);
        Cone3.transform.Rotate(Vector3.left, Time.deltaTime * RotationSpeed);
        Cone3.transform.Rotate(Vector3.forward, Time.deltaTime * RotationSpeed);
        Cone3.transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed);
        Cube1.transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed / 4.0f);

        if (_isFalling)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(0, 0, 0), Time.deltaTime/10.0f);
        }
    }

    public void SpawnNew()
    {
        _Spawner.SpawnToken();
    }
    public void Destroy()
    {
           photonView.RPC("_DestroyToken", PhotonTargets.All, GetComponent<PhotonView>().viewID);
    }
    private void OnValidate()
    {
        ValidateReferences();
    }

    #region Validation
    private void ValidateReferences()
    {
        if (_Spawner == null)
        {
            var sources = Resources.FindObjectsOfTypeAll<Spawner>();
            if (sources == null || sources.Length == 0)
            {
                Debug.LogError("No Spawner sources found");
            }
            else
            {
                _Spawner = sources[0];
            }
        }
    }
    #endregion Validation

    [PunRPC]
    private void _DestroyToken(int gameObjectID)
    {
        PhotonView.Destroy(PhotonView.Find(gameObjectID).gameObject);
    }
}
