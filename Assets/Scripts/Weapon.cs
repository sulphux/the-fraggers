﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{
    public float _ProjectileSpeed = 20.0f;

    public GameObject _Player;

    void Start ()
    {

    }
    
    void Update ()
    {

    }
    

    #region Public Methods
    public void Shoot(Ray ray)
    {
        
        var obj = PhotonNetwork.Instantiate("Projectile", transform.position, transform.rotation, 0);

        var projectile = obj.GetComponent<Projectile>();

        Physics.IgnoreCollision(obj.GetComponent<Collider>(), GetComponentInParent<Collider>());

        projectile.Direction = ray.direction * _ProjectileSpeed;

        projectile.AssignPlayerTransform(_Player);
    }
    #endregion Public Methods
}