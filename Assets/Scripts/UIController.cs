using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

    [SerializeField]
    private GameObject _IconToken;
    [SerializeField]
    private GameObject _IconTeleportGun;
    [SerializeField]
    private GameObject _DebugTextObject;

    [SerializeField]
    private Text _ScoreCounter;
    [SerializeField]
    private Text _DebugText;

	// Use this for initialization
	void Awake () {
    }   
	
	// Update is called once per frame
	void Update () {
	}

    public void Initialize()
    {
        _IconToken.SetActive(true);
        _IconTeleportGun.SetActive(true);
        //_DebugTextObject.SetActive(true);

        SetDebugText(PlayerPrefs.GetString("playerName"));
    }

    public void SetScore(int x)
    {
        _ScoreCounter.text = x.ToString();
    }

    public void SetDebugText(string txt)
    {
        _DebugText.text = txt;
    }
}
