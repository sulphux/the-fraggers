using UnityEngine;
using System.Collections;

public class Projectile : Photon.PunBehaviour
{
    [SerializeField] private GameObject _Explosion;
    
    public PhotonView projectileView;

    public float _Radius = 2.0f;
    public float _Force = 20.0f;
    public float _Life = 10.0f;

    public Vector3 Direction;

    private Rigidbody _Rigidbody;

    private GameObject _Player;

    // Use this for initialization
    void Start ()
    {
        _Rigidbody = GetComponent<Rigidbody>();

        _Rigidbody.AddForce(Direction, ForceMode.Impulse);
    }
    
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Projectile collided with " + collider.gameObject.name);
        //if ( collider.gameObject.name != "Player(Clone)" )
        _Life = -1.0f;
        PhotonNetwork.Instantiate("Explosion", transform.position, transform.rotation, 0);
        
    }

    // Update is called once per frame
    void Update ()
    {
        //not needed, transforming into grenade
        //transform.position += Velocity * Time.deltaTime;

        
        _Life -= Time.deltaTime;
        if(_Life < 0.0f)
        {
            Explode();
            if(photonView.isMine)
                PhotonNetwork.Destroy(projectileView);
        }
    }

    void FixedUpdate()
    {

    }

    public void AssignPlayerTransform(GameObject player)
    {
        _Player = player;
    }

    private void Explode()
    {
        var colliders = Physics.OverlapSphere(transform.position, _Radius);

        foreach (var collider in colliders)
        {
            var networkPlayer = collider.GetComponent<NetworkPlayerController>();
            if (networkPlayer != null)
            {
                networkPlayer.AffectExplosion(transform.position, _Radius, _Force);
            }
        }

        Vector3 newPosition = GetComponent<Transform>().position;

        _Player.transform.position = newPosition + newPosition.normalized * 1.0f;
    }
}
