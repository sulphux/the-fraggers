using UnityEngine;

/// <summary>
/// Allows component with rigidbody to be attracted by GravitySource
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class GravityBody : MonoBehaviour
{
    #region Inspector Variables
    [SerializeField]
    private GravitySource _Source;
    #endregion Inspector Variables

    #region Public Variables
    public Rigidbody Rigidbody;
    #endregion Public Variables

    #region Unity Messages

    private void Awake()
    {
        ValidateReferences();
    }

    private void FixedUpdate()
    {
        // Tell gravity source to pull this object
        _Source.Affect(this);
    }

    /// <summary>
    /// Called on components creation and on Reset in inspector
    /// </summary>
    private void Reset()
    {
        ValidateReferences();
    }

    /// <summary>
    /// Called each time any variable is changed in Inspector on this component
    /// </summary>
    private void OnValidate()
    {
        ValidateReferences();
    }

    #endregion Unity Messages

    #region Validation
    private void ValidateReferences()
    {
        if (Rigidbody == null)
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        if(_Source == null)
        {
            var sources = Resources.FindObjectsOfTypeAll<GravitySource>();
            if(sources == null || sources.Length == 0)
            {
                Debug.LogError("No gravity sources found");
            }else
            {
                _Source = sources[0];
            }
        }
    }
    #endregion Validation
}
