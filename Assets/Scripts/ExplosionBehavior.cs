﻿using UnityEngine;
using System.Collections;

public class ExplosionBehavior : MonoBehaviour {

    [SerializeField]
    private ParticleSystem _ParticleSystem;

	// Use this for initialization
	void Start () {
        _ParticleSystem.Play();
	}
	
	// Update is called once per frame
	void Update () {
        if (_ParticleSystem.isStopped)
        {
            PhotonNetwork.Destroy(gameObject);
        }
            
	}
}
