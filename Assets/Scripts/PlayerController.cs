using UnityEngine;

/*
* This class is responsible handling all player actions (animations, movement, input, jumping, collisions) except of gravity attraction.
* Rigidbody is required as sibling component and will be automatically detected and set in editor via OnValidate and Reset messages.
*
* All fields are private but allowed to be serialized and seen in inspector because of SerializeField attribute.
* [Header()] attribute allows to add labels before displaying field in inspector.
* [RequireComponent()] automatically adds specified component to this gameObject, if there is none of such type
*/

/// <summary>
/// Handles  movement, animations and collisions of Player
/// </summary>
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    #region Inspector Variables
    [Header("References")]
    [SerializeField] private PlayerInput    _Input;
    [SerializeField] private PlayerAnimator _Animator;
    [SerializeField] private Rigidbody      _Rigidbody;
    [SerializeField] private Camera         _Camera;
    [SerializeField] private Weapon         _Weapon;

    [Header("Settings")]
    [SerializeField] private float _WalkForce = 7.0f;
    [SerializeField] private float _RunForce  = 10.0f;
    [SerializeField] private float _JumpForce = 10.0f;
    [SerializeField] private float _TiltUp    = 80.0f;
    [SerializeField] private float _TiltDown  = 80.0f;

    [Header("Gizmos")]
    [SerializeField] private float _FlagDistance;
    [SerializeField] private LayerMask _FlagMask;

    [Header("Preview")]
    [SerializeField] private bool _OnGround = false;
    #endregion Inspector Variables

    #region Private Variables
    private Ray _FlagRay;
    #endregion Private Variables

    public string PlayerName; 

    #region Unity Messages
    private void Start()
    {
        LockCursor();
        PlayerName = PlayerPrefs.GetString("playerName");
    }
    private void Update()
    {
        UpdateAnimations();

        Flag();
        Shoot();
    }
    private void FixedUpdate()
    {
        DoRotate();
        DoMove();
        DoJump();
        ExitGamePressed();
    }

    public void OnCollisionStay(Collision collision)
    {
        // If player hit anything marked with layer "Environment"
        if (collision.gameObject.layer == LayerMask.NameToLayer("Environment"))
        {
            _OnGround = true;
        }
    }

    public void OnDrawGizmos()
    {
        _FlagRay = new Ray(transform.position + transform.forward * _FlagDistance, -transform.up);
        Gizmos.DrawLine(_FlagRay.origin, _FlagRay.origin + _FlagRay.direction * 2.0f);

        var ray = _Camera.ScreenPointToRay(Input.mousePosition);

        Gizmos.color = Color.red;
        Gizmos.DrawRay(ray);
    }

    /// <summary>
    /// Called on components creation and on Reset in inspector
    /// </summary>
    private void Reset()
    {
        ValidateReferences();
    }

    /// <summary>
    /// Called each time any variable is changed in Inspector on this component
    /// </summary>
    private void OnValidate()
    {
        ValidateReferences();
    }
    #endregion Unity Messages

    #region Private Methods
    private void UpdateAnimations()
    {
        // Update animation parameters
        if(_Input.Run)
        {
            _Animator.Run();
        }
        else
        {
            _Animator.Walk();
        }

        _Animator.Speed(_Input.Forward);
    }

    private void ExitGamePressed()
    {
        if(_Input.ExitGame)
        {
            Application.Quit();
        }
    }

    private void DoMove()
    {
        Vector3 translation = _Input.Forward * transform.forward;
        translation *= _Input.Run ? _RunForce : _WalkForce;

        _Rigidbody.AddForce(translation, ForceMode.Force);
    }
    private void DoRotate()
    {
        transform.Rotate(Vector3.up * _Input.Horizontal * Time.deltaTime);

        var rotation =_Camera.transform.localRotation * Quaternion.Euler(Vector3.right * - _Input.Vertical);
        if(rotation.eulerAngles.x < _TiltDown || rotation.eulerAngles.x > (360.0f - _TiltUp))
        {
            _Camera.transform.localRotation = rotation;
        }            
    }
    private void DoJump()
    {
        // Jumping
        if (_Input.Jump && _OnGround)
        {
            Debug.Log("Inside jumping!");
            _Rigidbody.AddForce(transform.up * _JumpForce, ForceMode.Impulse);
        }

        // Reset flag and wait for collision to occur
        _OnGround = false;
    }

    private void Flag()
    {
        _FlagRay = new Ray(transform.position + transform.forward * _FlagDistance, -transform.up);

        if(!_Input.Flag)
            return;

        if(Physics.Raycast(_FlagRay, 2.0f, _FlagMask))
        {
            _Animator.Flag();
        }
    }
    private void Shoot()
    {
        if(_Input.Shoot)
        {
            // OLD Ray from course
            //var ray = _Camera.ScreenPointToRay(Input.mousePosition); 

            Vector3 fwd = _Camera.transform.TransformDirection(Vector3.forward);
            Ray ray = new Ray(_Camera.transform.position, fwd);

            _Weapon.Shoot(ray);
        }
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    #endregion Private Methods

    #region Validation Methods
    private void ValidateReferences()
    {
        FindReferenceInHierarchy(ref _Camera);

        FindReferenceInHierarchy(ref _Rigidbody);
        FindReferenceInHierarchy(ref _Animator);

        FindReferenceInHierarchy(ref _Input);
        FindReferenceInHierarchy(ref _Weapon);
    }
    #endregion Validation Methods

    #region Helper Methods
    /// <summary>
    /// Given parameter of type T, find component in GameObject or its children
    /// <T> is a declaration of generic method, where T is a type resolved from usage
    /// </summary>
    private void FindReferenceInHierarchy<T>(ref T reference) where T : class
    {
        if(!reference.Equals(null))
        {
            return;
        }

        reference = GetComponent<T>();
        if(!reference.Equals(null))
        {
            return;
        }

        reference = GetComponentInChildren<T>();

        if(reference.Equals(null))
        {
            Debug.LogError("Reference to object: " + typeof(T).Name + " not found");
        }
    }
    #endregion Helper Methods
}
