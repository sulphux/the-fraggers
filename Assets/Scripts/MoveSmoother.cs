﻿using UnityEngine;
using System.Collections;

public class MoveSmoother : Photon.PunBehaviour
{
    public float lerpSpeedFactor = 5f;

    private Vector3 correctPosition;
    private Quaternion correctRotation;

    void Update()
    {
        if (!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, correctPosition, Time.deltaTime * lerpSpeedFactor);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctRotation, Time.deltaTime * lerpSpeedFactor);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            correctPosition = (Vector3)stream.ReceiveNext();
            correctRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
