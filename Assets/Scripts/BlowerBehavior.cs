﻿using UnityEngine;
using System.Collections;

public class BlowerBehavior : MonoBehaviour {

    [SerializeField] private float _BlowForce = 10.0f;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Blower triggered!");
        Rigidbody rg = other.GetComponent<Rigidbody>();
        Vector3 pos = other.GetComponent<Transform>().position;

        rg.AddForce(pos.normalized * _BlowForce, ForceMode.Impulse);
    }
}
