using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(NetworkPlayerSkirmish))]
public class NetworkPlayerController : Photon.PunBehaviour
{
    public float lerpSpeedFactor = 5f;

    private Vector3 correctPosition;
    private Quaternion correctRotation;

    private Rigidbody _rigidbody;

    public Transform _LabelTransform;
    public Text _LabelText;
    public GameObject _LabelTextObj;

    [SerializeField]
    private Camera _MainCamera;


    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();

        if(photonView.isMine)
        {
            GetComponent<PlayerController>().enabled = true;
            GetComponent<PlayerInput>().enabled = true;
            GetComponentInChildren<Camera>().enabled = true;
            
            GameObject.FindGameObjectWithTag("WeaponCamera").GetComponentInChildren<Camera>().enabled = true;
            _LabelTextObj.SetActive(true);
        }
    }


    void Update()
    {
        if(!photonView.isMine)
        {
            transform.position = Vector3.Lerp(transform.position, correctPosition, Time.deltaTime * lerpSpeedFactor);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctRotation, Time.deltaTime * lerpSpeedFactor);
            _LabelTransform.rotation = Camera.main.transform.rotation;

        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            correctPosition = (Vector3)stream.ReceiveNext();
            correctRotation = (Quaternion)stream.ReceiveNext();
        }
    }

    public void AffectExplosion(Vector3 position, float radius, float force)
    {
        if(Vector3.Distance(position, transform.position) < radius)
            photonView.RPC("Explosion", PhotonTargets.All, position, radius, force);
    }

    [PunRPC]
    void Explosion(Vector3 position, float radius, float force)
    {
        _rigidbody.AddExplosionForce(force, position, radius);
    }

    [PunRPC]
    void SetPlayerName(string name)
    {
        _LabelText.text = name;
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        base.OnPhotonPlayerConnected(newPlayer);

        if (photonView.isMine)
            photonView.RPC("SetPlayerName", PhotonTargets.All, PlayerPrefs.GetString("playerName"));

    }
}
