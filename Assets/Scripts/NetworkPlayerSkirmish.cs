using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NetworkPlayerSkirmish : Photon.PunBehaviour
{
    [SerializeField]
    private int _Score;

    private UIController _UIController;

    void Awake()
    {
        ResetScore();
        
        if (photonView.isMine)
        {
            _UIController = GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIController>();

            _UIController.Initialize();
        }
    }

    void Update()
    {

        _UIController.SetScore(_Score);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Token"))
        {
            Debug.Log("Colliding with :"+other.gameObject.name + " Tag:" + other.gameObject.tag);
            //AddScore();
            //other.GetComponent<TokenBehavior>().SpawnNew();
            //photonView.RPC("_DestroyToken", PhotonTargets.All, other.GetComponent<PhotonView>().viewID);
            //PhotonNetwork.Destroy(other.gameObject);
            //Destroy(other.gameObject);
            other.GetComponent<TokenBehavior>().Destroy();
            PhotonNetwork.Destroy(other.gameObject);
        }
    }

    public void AddScore()
    {
        _Score++;
    }

    public void ResetScore()
    {
        _Score = 0;
    }
}
