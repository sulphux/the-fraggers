﻿using UnityEngine;
using System.Collections;

public class Matchmaker : Photon.PunBehaviour
{
    public string version;

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings(version);
    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        base.OnPhotonRandomJoinFailed(codeAndMsg);

        Debug.Log("Can't join random room! Tryin' to create new room");

        PhotonNetwork.CreateRoom(null);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Debug.Log("Room joined successfully!");
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();

        Debug.Log("Room succesfully created!");
    }
}
